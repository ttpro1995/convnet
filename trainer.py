from torch.autograd import Variable
import torch.nn as nn
import torch
class Trainer:
    def __init__(self, cuda, model, embedding_model, optimizer, dataset, batchsize = 25):
        self.cudaFlag = cuda
        self.model = model
        self.embedding_model = embedding_model
        self.optimizer = optimizer
        self.dataset = dataset
        self.criterion = nn.CrossEntropyLoss()
        self.batchsize = batchsize

    def train(self, train_idx):
        self.model.train()
        self.embedding_model.train()
        self.optimizer.zero_grad()
        total_loss = 0
        batch_counter = 0 # counter number sample in batch
        for idx in train_idx:
            paragraph, label = self.dataset[idx]
            emb_paragraph = []
            if len(paragraph) <1:
                print('empty sample %d'%(idx))
                continue
            for sent in paragraph:
                in_sent = Variable(sent).long()
                target = Variable(torch.Tensor([label])).long()
                if self.cudaFlag:
                    in_sent = in_sent.cuda()
                    target = target.cuda()
                sent_emb = self.embedding_model(in_sent)
                sent_emb = sent_emb.unsqueeze(1)
                emb_paragraph.append(sent_emb)
            output = self.model(emb_paragraph)
            loss = self.criterion(output, target)
            loss.backward()
            total_loss += loss.data[0] # float loss to display
            batch_counter +=1
            if batch_counter == self.batchsize:
                self.optimizer.step()
                self.optimizer.zero_grad()
                batch_counter = 0
        # step last time for those not make up full batch
        self.optimizer.step()
        self.optimizer.zero_grad()
        batch_counter = 0
        average_loss = total_loss/len(train_idx)
        return average_loss

    def test(self, test_idx):
        self.model.eval()
        self.embedding_model.eval()
        predictions = []
        for idx in test_idx:
            paragraph, label = self.dataset[idx]
            emb_paragraph = []
            for sent in paragraph:
                in_sent = Variable(sent).long()
                target = Variable(torch.Tensor([label])).long()
                if self.cudaFlag:
                    in_sent = in_sent.cuda()
                    target = target.cuda()
                sent_emb = self.embedding_model(in_sent)
                sent_emb = sent_emb.unsqueeze(1)
                emb_paragraph.append(sent_emb)
            output = self.model(emb_paragraph)
            loss = self.criterion(output, target)
            _, predict = torch.max(output, 1)
            predictions.append(predict)
        predictions = torch.cat(predictions, 0)
        return loss, predictions