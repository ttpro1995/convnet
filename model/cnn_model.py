import torch
import torch.nn as nn
from torch.autograd import Variable
import torch.nn.functional as F

def kmax_pooling(cuda, x, dim, k):
    padding = Variable(torch.zeros((x.size(0), x.size(1), k)))
    if cuda:
        padding = padding.cuda()
    if k > x.size(2):
        x = torch.cat([x, padding], 2)
    index = x.topk(k, dim = dim)[1].sort(dim = dim)[0]
    return x.gather(dim, index)

class ConvNetPipeline(nn.Module):
    def __init__(self, cuda, emb_dim, kernel_size, n_filter, kmax):
        super(ConvNetPipeline, self).__init__()
        self.cudaFlag = cuda
        self.snet = ConvNet(cuda, emb_dim, kernel_size, n_filter, kmax)
        self.pnet = ConvNet(cuda, n_filter*kmax, kernel_size, n_filter, kmax)
        self.output_module = SentimentModule(cuda, n_filter* kmax, 3, dropout=True)
        self.n_filter = n_filter
        self.kmax = kmax
        self.kernel_size = kernel_size

    def forward(self, paragraph):
        o = []
        for sent in paragraph: # sent size (seq, 1, 300)
            output = self.snet(sent) #(1, 1, n_filter*kmax)
            o.append(output)
        p_input = torch.cat(o)
        p_o = self.pnet(p_input) # (5, 1, 500)
        p_o = p_o.squeeze(0)
        output = self.output_module(p_o)
        return output


class ConvNet(nn.Module):
    def __init__(self, cuda, emb_dim, kernel_size, n_filter, kmax):
        super(ConvNet, self).__init__()
        self.cudaFlag = cuda
        self.kernel_size = kernel_size
        self.n_filter = n_filter
        self.kmax = kmax
        self.conv = nn.Conv2d(1, n_filter, (kernel_size, emb_dim), padding=(kernel_size-1, 0))
        if self.cudaFlag:
            self.conv = self.conv.cuda()

    def forward(self, sent_emb):
        """
        :param sent_emb: embedding of a sentence (seq_len, 1, embedding_dim)
        :return: 
        """
        tmp_vec = sent_emb.unsqueeze(2)
        tmp_vec = torch.transpose(tmp_vec, 0, 2)
        out_vec = self.conv(tmp_vec) # (1, n_filter, L, 1)
        out_vec = out_vec.squeeze(3) # (1, n_filter, L)
        out_vec = kmax_pooling(self.cudaFlag, out_vec, 2, self.kmax) # (1, n_filter, kmax)
        out_vec = out_vec.view(1, 1, self.n_filter*self.kmax) # (1, 1, kmax*n_filter)
        return out_vec

# output module
class SentimentModule(nn.Module):
    def __init__(self, cuda, mem_dim, num_classes, dropout = False):
        super(SentimentModule, self).__init__()
        self.cudaFlag = cuda
        self.mem_dim = mem_dim
        self.num_classes = num_classes
        self.dropout = dropout
        # torch.manual_seed(456)
        self.l1 = nn.Linear(self.mem_dim, self.num_classes)
        self.logsoftmax = nn.LogSoftmax()
        if self.cudaFlag:
            self.l1 = self.l1.cuda()

    def forward(self, vec, training = False):
        if self.dropout:
            out = self.logsoftmax(self.l1(F.dropout(vec, training = training)))
        else:
            out = self.logsoftmax(self.l1(vec))
        return out

if __name__ == '__main__':
    # net = ConvNet(0, 300, 3, 2, 5)
    # paragraph = []
    # paragraph.append(Variable(torch.rand(24,1,300)))
    # paragraph.append(Variable(torch.rand(13, 1, 300)))
    # paragraph.append(Variable(torch.rand(25, 1, 300)))
    # paragraph.append(Variable(torch.rand(15, 1, 300)))
    # paragraph.append(Variable(torch.rand(17, 1, 300)))
    # o = []
    # for sent in paragraph:
    #     output = net(sent)
    #     o.append(output)
    # for s in o:
    #     print (s.size())
    # oo = torch.cat(o)
    # net2 = ConvNet(0, 10, 3, 3, 5)
    # print (oo.size())
    # o2 = net2(oo)
    # print(o2.size())
    # print('break')

    paragraph = []
    paragraph.append(Variable(torch.rand(24,1,300)))
    paragraph.append(Variable(torch.rand(13, 1, 300)))
    paragraph.append(Variable(torch.rand(25, 1, 300)))
    paragraph.append(Variable(torch.rand(15, 1, 300)))
    paragraph.append(Variable(torch.rand(17, 1, 300)))
    net_pipe = ConvNetPipeline(0, 300, 5, 3, 5)
    o = net_pipe(paragraph)
    print('new o')
    # for s in o:
    #     print(s.size())
    print(o.size())
    print(o)