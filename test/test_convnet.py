import pytest
import torch
import torch.nn as nn
from torch.autograd import Variable
from model import ConvNet, ConvNetPipeline

def test_paragraph():

    paragraph = []
    paragraph.append(Variable(torch.rand(24, 1, 300))) # a sentences
    paragraph.append(Variable(torch.rand(13, 1, 300)))
    paragraph.append(Variable(torch.rand(25, 1, 300)))
    paragraph.append(Variable(torch.rand(15, 1, 300)))
    paragraph.append(Variable(torch.rand(17, 1, 300)))
    o = []

    net = ConvNet(0, 300, 3, 2, 5)
    for sent in paragraph:
        output = net(sent)
        o.append(output)
    for s in o:
        print (s.size())
    oo = torch.cat(o)
    net2 = ConvNet(0, 10, 5, 100, 5)
    print (oo.size())
    o2 = net2(oo)
    print(o2.size())
    print('break')

def test_pipeline():
    paragraph = []
    paragraph.append(Variable(torch.rand(24, 1, 300)))  # a sentences
    paragraph.append(Variable(torch.rand(13, 1, 300)))
    paragraph.append(Variable(torch.rand(25, 1, 300)))
    paragraph.append(Variable(torch.rand(15, 1, 300)))
    paragraph.append(Variable(torch.rand(17, 1, 300)))
    o = []

    pipe = ConvNetPipeline(1, 300, 3, 100, 5)

    o2 = pipe(paragraph)
    print(o2.size())
    print('break')