from datautil import load_word_vectors
from datautil import Dataset
import torch
import torch.nn as nn
def test_convnetemb():
    # load glove embeddings and vocab
    ds = Dataset.read_file_text('../test_dataset', 'movie')
    vocab = ds.vocab
    emb_vector_path = '../test_dataset/convnetemb'
    emb_split_token = ' '
    cuda = torch.cuda.is_available()
    glove_vocab, glove_emb = load_word_vectors(emb_vector_path, emb_split_token)
    print('==> Embedding vocabulary size: %d ' % glove_vocab.size())

    emb = torch.zeros(vocab.size(), glove_emb.size(1))

    for word in vocab.labelToIdx.keys():
        if glove_vocab.getIndex(word):
            emb[vocab.getIndex(word)] = glove_emb[glove_vocab.getIndex(word)]
        else:
            emb[vocab.getIndex(word)] = torch.Tensor(emb[vocab.getIndex(word)].size()).normal_(-0.05, 0.05)

    embedding_model = nn.Embedding(vocab.size(), 300)
    if cuda:
        embedding_model = embedding_model.cuda()
        emb = emb.cuda()
    embedding_model.state_dict()['weight'].copy_(emb)