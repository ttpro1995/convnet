import pytest
import os
from datautil.dataset import Dataset
from datautil.dataset import build_vocab


def test_dataset():
    '''test on class'''
    test_data_dir = '../test_dataset'
    file_list = ['text.txt', 'text2.txt']
    file_path = []
    for f in file_list:
        path = os.path.join(test_data_dir, f)
        file_path.append(path)
    print 'meow'

    build_vocab(file_path, 'text.vocab')
    f2 = open(os.path.join(test_data_dir,'text2.txt'), 'r')
    f1 = open(os.path.join(test_data_dir, 'text.txt'), 'r')
    ds = Dataset('text.vocab')
    for line in f1.readlines():
        ds.read_sentence(line, 1)
    for line in f2.readlines():
        ds.read_sentence(line, 2)

    for i in xrange(len(ds.label)):
        sentence = ' '.join(ds.vocab.convertToLabels(ds.sentences[i], -1))
        label = ds.label[i]
        print sentence, label

def test_create_dataset():
    ds = Dataset.create_dataset('../test_dataset', 'movie')
    ds.save('../test_dataset', 'movie')
    new_ds = Dataset.load('../test_dataset', 'movie')
    ds.print_statistic()
    new_ds.print_statistic()
    for i in xrange(len(ds.label)):
        sentence = ' '.join(ds.vocab.convertToLabels(ds.sentences[i], -1))
        label = ds.label[i]
        print sentence, label
    print ('-------------')
    for i in xrange(len(new_ds.label)):
        sentence = ' '.join(new_ds.vocab.convertToLabels(new_ds.sentences[i], -1))
        label = new_ds.label[i]
        print sentence, label

def test_read_txt_pool():
    "Read text file by parallel"
    print ('test_read_txt_pool()')
    ds = Dataset.read_file_text('../test_dataset','movie')
    for paragraph in ds.paragraph_list:
        sentence = ''
        for sent in paragraph:
            sentence += ' '+ ' '.join(ds.vocab.convertToLabels(sent, -1))
        print (sentence)

    print('done')