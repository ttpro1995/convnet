import pytest
from trainer import Trainer
from datautil import Dataset
from model import ConvNetPipeline
import torch.nn as nn
from torch import optim
import torch

def test_train():
    ds = Dataset.read_file_text('../test_dataset', 'movie')
    cuda = 1
    vocab = ds.vocab
    embedding_model = nn.Embedding(vocab.size(), 300)
    if cuda:
        embedding_model = embedding_model.cuda()
    model = ConvNetPipeline(cuda, 300, 3, 100, 5)
    print (model)

    optimizer = optim.Adam([
        {'params': model.parameters(), 'lr': 0.001, 'weight_decay': 1e-6},
        {'params': embedding_model.parameters(), 'lr': 0.001, 'weight_decay': 1e-6}
    ])

    trainer = Trainer(cuda, model, embedding_model, optimizer, ds)
    loss = trainer.train(range(1,5))
    for i in range(3):
        loss = trainer.train(range(0,10))
        print ('loss %f '%(loss))

def test_test():
    ds = Dataset.read_file_text('../test_dataset', 'movie')
    cuda = 1
    vocab = ds.vocab
    embedding_model = nn.Embedding(vocab.size(), 300)
    if cuda:
        embedding_model = embedding_model.cuda()
    model = ConvNetPipeline(cuda, 300, 3, 100, 5)
    print (model)

    optimizer = optim.Adam([
        {'params': model.parameters(), 'lr': 0.001, 'weight_decay': 1e-6},
        {'params': embedding_model.parameters(), 'lr': 0.001, 'weight_decay': 1e-6}
    ])

    trainer = Trainer(cuda, model, embedding_model, optimizer, ds)
    loss, pred = trainer.test(range(1,5))
    print('lost')
    print (loss)
    print (pred)

def test_it_should_train():
    ds = Dataset.read_file_text('../test_dataset', 'movie')
    cuda = torch.cuda.is_available()
    vocab = ds.vocab
    embedding_model = nn.Embedding(vocab.size(), 300)
    if cuda:
        embedding_model = embedding_model.cuda()
    model = ConvNetPipeline(cuda, 300, 3, 100, 5)
    print (model)

    optimizer = optim.Adam([
        {'params': model.parameters(), 'lr': 0.001, 'weight_decay': 1e-6},
        {'params': embedding_model.parameters(), 'lr': 0.001, 'weight_decay': 1e-6}
    ])

    trainer = Trainer(cuda, model, embedding_model, optimizer, ds)
    loss = trainer.train(range(1,5))
    for i in range(100):
        loss = trainer.train(range(0,10))
        print ('loss %f '%(loss))
    loss, pred = trainer.test(range(0,10))
    print (pred)
    if cuda:
        ds.paragraph_label = ds.paragraph_label.cuda()
    correct = pred.data.squeeze() == ds.paragraph_label.long()
    print (correct)
    assert sum(correct) == 10 # it should get 100% accuracy on all 10 sample