from vocab import Vocab
import os
import pickle
from vocab import build_vocab
from tqdm import tqdm
from joblib import Parallel, delayed
import torch
import nltk
from multiprocessing import Pool
from joblib import Parallel, delayed

def unwrap(i):
    '''
    For multithreading
    :param i: self-Dataset instance, line-paragraph
    :return:
    '''
    self, line = i
    paragraph = self.parse_paragraph(line)
    return paragraph





class Dataset():
    def __init__(self, vocab_file):
        self.vocab = Vocab(filename=vocab_file)
        self.sentences = []
        self.paragraph_list = []
        self.paragraph_label = []
        self.label = []

    def save(self, path, dsname):
        with open(os.path.join(path, dsname+'sentences.p'), 'wb') as file:
            pickle.dump(self.sentences, file, pickle.HIGHEST_PROTOCOL)
        with open(os.path.join(path, dsname+'labels.p'), 'wb') as file:
            pickle.dump(self.label, file, pickle.HIGHEST_PROTOCOL)

    def __getitem__(self, key):
        # It's probably better to catch any IndexError to at least provide
        # a class-specific exception
        return self.paragraph_list[key], self.paragraph_label[key]

    def __len__(self):
        return len(self.paragraph_label)

    @staticmethod
    def load(path, dsname):
        vocab_file = os.path.join(path, dsname+'.vocab')
        ds = Dataset(vocab_file)
        with open(os.path.join(path, dsname+'sentences.p'), 'rb') as file:
            ds.sentences = pickle.load(file)
        with open(os.path.join(path, dsname+'labels.p'), 'rb') as file:
            ds.label = pickle.load(file)
        return ds

    @staticmethod
    def read_file_text(path, dsname):
        '''
        Read pos, neg, neu, vocab from given path
        :param path: directory to look for data
        :param dsname: prefix name => dsname.pos
        :return:
        '''
        vocab_file = os.path.join(path, dsname + '.vocab')
        pos = open(os.path.join(path, dsname + '.pos'), 'r')
        neg = open(os.path.join(path, dsname + '.neg'), 'r')
        neu = open(os.path.join(path, dsname + '.neu'), 'r')
        ds = Dataset(vocab_file)
        ds.vocab.add('<unk>')

        # load those in memor
        pos_lines = pos.readlines()
        neg_lines = neg.readlines()
        neu_lines = neu.readlines()

        # multiprocessing
        p = Pool(5)

        pos_payload = zip([ds] * len(pos_lines), pos_lines)
        neg_payload = zip([ds] * len(neg_lines), neg_lines)
        neu_payload = zip([ds] * len(neu_lines), neu_lines)
        pos_paragraphs_indices = Parallel(n_jobs=6,  backend="threading", verbose=True)(delayed(unwrap)(i) for i in pos_payload)
        neg_paragraphs_indices = Parallel(n_jobs=6,  backend="threading",  verbose=True)(delayed(unwrap)(i) for i in neg_payload)
        neu_paragraphs_indices = Parallel(n_jobs=6,  backend="threading",  verbose=True)(delayed(unwrap)(i) for i in neu_payload)
        # pos_paragraphs_indices = p.map(unwrap, pos_payload)
        # neg_paragraphs_indices = p.map(unwrap, neg_payload)
        # neu_paragraphs_indices = p.map(unwrap, neu_payload)
        ds.paragraph_list.extend(pos_paragraphs_indices)
        ds.paragraph_label.extend([2] * len(pos_paragraphs_indices))
        ds.paragraph_list.extend(neg_paragraphs_indices)
        ds.paragraph_label.extend([0] * len(neg_paragraphs_indices))
        ds.paragraph_list.extend(neu_paragraphs_indices)
        ds.paragraph_label.extend([1] * len(neu_paragraphs_indices))
        ds.paragraph_label = torch.Tensor(ds.paragraph_label)
        return ds

    def print_statistic(self):
        pos_label = 0
        neg_label = 0
        neu_label = 0
        total_len = len(self.paragraph_label)
        for l in self.paragraph_label:
            if l == 2:
                pos_label+=1
            elif l ==0:
                neg_label +=1
            elif l == 1:
                neu_label +=1
        print ('total %d'%(total_len))
        print('pos %d '%(pos_label))
        print('neg %d '%(neg_label))
        print('neu %d'%(neu_label))

    def read_sentence(self, sentence, label = -1):
        indices = self.vocab.convertToIdx(sentence.split(), '<unk>')
        self.sentences.append(indices)
        self.label.append(label)
        return indices



    @staticmethod
    def create_dataset(data_path, data_name):
        vocab_file = os.path.join(data_path, data_name+'.vocab')
        pos_file = os.path.join(data_path, data_name+'.pos')
        neg_file = os.path.join(data_path, data_name + '.neg')
        neu_file = os.path.join(data_path, data_name + '.neu')
        ds = Dataset(vocab_file)

        with open(pos_file, 'r') as file:
            for line in tqdm(file):
                ds.read_sentence(line, 2)

        with open(neg_file, 'r') as file:
            for line in tqdm(file):
                ds.read_sentence(line, 0)

        with open(neu_file, 'r') as file:
            for line in tqdm(file):
                ds.read_sentence(line, 1)
        return ds

    def parse_sentences(self, sentence):
        '''
        Turn sentences into list of indices
        :param sentence: a sentences (in raw word)
        :return: list of indices
        '''
        indices = self.vocab.convertToIdx(sentence.split(), '<unk>')
        if indices is None:
            indices = self.vocab.labelToIdx['unk']
        indices = torch.Tensor(indices)
        return indices

    def parse_paragraph(self, line):
        '''
        Take a raw sencenes, turn sentences into indices
        :param line: a paragraph
        :return: a list, each element is list of sentences indices
        '''
        # get list of sentences
        sentences = nltk.sent_tokenize(line)
        paragraph=[]
        for sent in sentences:
            indices = self.parse_sentences(sent)
            paragraph.append(indices)
        return paragraph