import os
from config import parsearg
from datautil import Vocab
from datautil import load_word_vectors
import torch
import torch.nn as nn
from torch import optim
from datautil import Dataset
import gc
from model import ConvNet, ConvNetPipeline
from trainer import Trainer
from sklearn.model_selection import train_test_split
from torch.autograd import Variable
import numpy as np
from util import vector_to_string
from meowlogtool import log_util
import sys

if __name__ == '__main__':
    args = parsearg()
    log_path = os.path.join(args.logs, args.name)
    logger1 = log_util.create_logger(log_path, print_console=True)
    logger1.info("LOG_FILE") # log using logger1

    print (args)
    logger1.info(args)

    print('start load dataset')
    # load dataset
    ds = Dataset.read_file_text(args.data, 'movie5000')
    ds.print_statistic()
    print('done load dataset')

    # embedding
    emb_torch = 'movie_embedding.pth'
    vocab = ds.vocab
    emb_vector = 'sorted20'
    emb_vector_path = os.path.join(args.embedding, emb_vector)
    emb_split_token = ' '
    emb_file = os.path.join(args.data, emb_torch)
    if os.path.isfile(emb_file):
        emb = torch.load(emb_file)
    else:
        # load glove embeddings and vocab
        glove_vocab, glove_emb = load_word_vectors(emb_vector_path, emb_split_token)
        print('==> Embedding vocabulary size: %d ' % glove_vocab.size())

        emb = torch.zeros(vocab.size(),glove_emb.size(1))

        for word in vocab.labelToIdx.keys():
            if glove_vocab.getIndex(word):
                emb[vocab.getIndex(word)] = glove_emb[glove_vocab.getIndex(word)]
            else:
                emb[vocab.getIndex(word)] = torch.Tensor(emb[vocab.getIndex(word)].size()).normal_(-0.05,0.05)
        # torch.save(emb, emb_file) file too big

    embedding_model = nn.Embedding(vocab.size(), args.embedding_dim)
    if args.cuda:
        embedding_model = embedding_model.cuda()
        emb = emb.cuda()
    embedding_model.state_dict()['weight'].copy_(emb)
    emb = None
    glove_emb = None
    gc.collect()
    model = ConvNetPipeline(args.cuda, args.embedding_dim, 5, 150, 5)


    # optimizer
    if args.optim=='adam':
        optimizer   = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.wd)
    elif args.optim=='adagrad':
        # optimizer   = optim.Adagrad(filter(lambda p: p.requires_grad, model.parameters()), lr=args.lr, weight_decay=args.wd)
        optimizer = optim.Adagrad(model.parameters(), lr=args.lr, weight_decay=args.wd)
    elif args.optim == 'adadelta':
        optimizer = optim.Adadelta(model.parameters(), lr = args.lr, weight_decay=args.wd)
    elif args.optim=='adam_combine':
        optimizer = optim.Adam([
                {'params': model.parameters(), 'lr':args.lr, 'weight_decay':args.wd },
                {'params': embedding_model.parameters(), 'lr': args.emblr, 'weight_decay':args.embwd}
            ])
        args.manually_emb = 0
    elif args.optim == 'adagrad_combine':
        optimizer = optim.Adagrad([
                {'params': model.parameters(), 'lr':args.lr, 'weight_decay':args.wd },
                {'params': embedding_model.parameters(), 'lr': args.emblr, 'weight_decay':args.embwd}
            ])
        args.manually_emb = 0
    elif args.optim == 'adam_combine_v2':
        model.embedding_model = embedding_model
        optimizer = optim.Adam(model.parameters(), lr=args.lr, weight_decay=args.wd)
        args.manually_emb = 0

    trainer = Trainer(args.cuda, model, embedding_model, optimizer, ds)


    # real training

    # train test split
    indices = range(len(ds))# idx of sample.
    indices = np.array(indices)
    train_indices, test_indices = train_test_split(indices, test_size=0.2)
    best_acc = 0
    for e in range(args.epochs):
        np.random.shuffle(indices)
        training_loss = trainer.train(train_indices)

        # test
        label = ds.paragraph_label[torch.from_numpy(test_indices)].long()
        if args.cuda:
            label = label.cuda()
        loss, pred = trainer.test(test_indices)
        correct = pred.data.squeeze() == label
        total = len(test_indices)
        acc = float(sum(correct))/total
        print ('training loss %f'%(training_loss))
        print('epoch %d acc %f'%(e, acc))
        logger1.info('epoch %d acc %f'%(e, acc))
        if acc > best_acc:
            torch.save(model, os.path.join(args.saved, 'model.pth'))
            torch.save(embedding_model, os.path.join(args.saved, 'embedding_model.pth'))


    best_model = torch.load(os.path.join(args.saved, 'model.pth'))
    best_embedding_model = torch.load(os.path.join(args.saved, 'embedding_model.pth'))

    # extract embedding model
    emb_save_text = open(os.path.join(args.saved, 'convnetemb.txt'), 'w')
    for i in vocab.idxToLabel.keys():
        word = vocab.idxToLabel[i]
        i_var = Variable(torch.Tensor([i])).long()
        if args.cuda:
            i_var = i_var.cuda()
        vector = best_embedding_model(i_var)
        s = word+' '+vector_to_string(vector)
        emb_save_text.write(s + '\n')
    emb_save_text.close()
    print ('ALL done')





    # # test only
    # for i in range(args.epochs):
    #     loss = trainer.train(range(5))
    #     print ('loss %f '%(loss))
    # loss, pred = trainer.test(range(5))
    # print (pred)
    # if args.cuda:
    #     ds.paragraph_label = ds.paragraph_label.cuda()
    # correct = pred.data.squeeze() == ds.paragraph_label[0:5].long()
    # print (sum(correct))



    print('all done')
    logger1.info('all done')
    html_log = log_util.up_gist(log_path+'.log', args.name, 'convnet')
    print('link on gist %s' % (html_log))
    logger1.info('link on gist %s' % (html_log))