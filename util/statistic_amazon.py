import json
import math
import nltk
from joblib import Parallel, delayed
import re
from tqdm import tqdm
import sys
def preprocess_review(review):
    '''
    Preprocess text and overall
    :param review: a json review
    :return:
    (text, overal)
    '''
    dt = json.loads(review)
    text = dt['reviewText']
    overall = dt['overall']
    overall = math.floor(overall)
    overall = int(overall)
    # solve case where sentence.sentence (no space after period)
    text = re.sub(r'([a-z|\d])[\.]+([A-Z|\d|0-9|-])', r'\1. \2', text)
    text = re.sub(r'([a-z|\d])[\,]+([A-Z|\d|0-9|-])', r'\1, \2', text)
    text = nltk.word_tokenize(text)
    sentence = ' '.join(text)
    sentences = nltk.sent_tokenize(sentence)
    n_sentence = len(sentences)
    return sentence, overall, n_sentence



def read_amazon_joblib():
    file_name = sys.argv[1]
    file = open(file_name, 'r')
    all_lines = file.readlines() # read whole file in RAM (well, we have 24 GB of ram)
    result = Parallel(n_jobs=7, verbose=1, backend="threading")(delayed(preprocess_review)(line) for line in all_lines)
    counter = 0
    overall_count = [0, 0, 0, 0, 0, 0]
    sentences_length_by_overall = [[], [], [],[], [], []]
    for sent, overall, n_sentence in tqdm(result):
        overall_count[overall] += 1
        sentences_length_by_overall[overall].append(n_sentence)
    result_file = open('result.txt', 'w')
    for i in overall_count:
        result_file.write(str(i)+'\n')
    result_file.write('---------------\n')
    for o in sentences_length_by_overall:
        for idx in o:
            result_file.write(str(idx)+' ')
        result_file.write('\n')
    result_file.close()

if __name__ == '__main__':
    read_amazon_joblib()
