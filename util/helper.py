import torch
def vector_to_string(vector):
    '''
    one dimension vector to string
    :param vector:
    :return:
    '''
    vector = vector.squeeze()
    s  = []
    for i in vector.data:
        s.append(str(i))
    s = ' '.join(s)
    return s