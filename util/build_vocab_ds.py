import os
import sys

from datautil import build_vocab

if __name__ == '__main__':
    # python build_vocab_ds ../data
    data_dir = sys.argv[1]
    print (data_dir)
    file_list = ['movie5000.neg', 'movie5000.pos', 'movie5000.neu']
    file_path = []
    for f in file_list:
        path = os.path.join(data_dir, f)
        file_path.append(path)
    print 'meow'

    build_vocab(file_path, 'movie5000.vocab', verbose=True)
    print 'all done'