# read amazon dataset
import sys
import json
import math
import nltk
import re
import time
from joblib import Parallel, delayed
from tqdm import tqdm
# TODO: implement preprocess using mutilthreading

def preprocess_review(review):
    '''
    Preprocess text and overall
    :param review: a json review
    :return:
    (text, overal)
    '''
    dt = json.loads(review)
    text = dt['reviewText']
    overall = dt['overall']
    overall = math.floor(overall)
    # solve case where sentence.sentence (no space after period)
    text = re.sub(r'([a-z|\d])[\.]+([A-Z|\d|0-9|-])', r'\1. \2', text)
    text = re.sub(r'([a-z|\d])[\,]+([A-Z|\d|0-9|-])', r'\1, \2', text)
    text = nltk.word_tokenize(text)
    sentence = ' '.join(text)
    return sentence, overall



def read_amazon_joblib():
    file_name = sys.argv[1]
    pos_file = open('movie.pos', 'a')
    neg_file = open('movie.neg', 'a')
    neu_file = open('movie.neu', 'a')
    file = open(file_name, 'r')
    all_lines = file.readlines() # read whole file in RAM (well, we have 24 GB of ram)
    result = Parallel(n_jobs=6, backend='threading' ,verbose=1)(delayed(preprocess_review)(line) for line in all_lines)
    counter = 0
    pos = 0
    neg = 0
    neu = 0
    for sent, overall in tqdm(result):
        writer = None  # file writer
        if overall < 3:
            writer = neg_file
            neg += 1
        elif overall > 3:
            writer = pos_file
            pos += 1
        elif overall == 3:
            writer = neu_file
            neu += 1

        writer.write(sent + ' ')
        writer.write('\n')
        counter += 1
        if counter % 100000 == 0:
            print counter

    print ('all done')
    print ('total ' + str(counter))
    print ('pos ' + str(pos))
    print ('neg ' + str(neg))
    print ('neu ' + str(neu))
    pos_file.close()
    neu_file.close()
    neu_file.close()




def old_reader():
    file_name = sys.argv[1]
    pos_file = open('movie.pos', 'a')
    neg_file = open('movie.neg', 'a')
    neu_file = open('movie.neu', 'a')
    print ('start')
    t1 = time.time()
    with open(file_name, 'r') as file:
        counter = 0
        pos = 0
        neg = 0
        neu = 0
        for line in file:
            dt = json.loads(line)
            text = dt['reviewText']
            overall = dt['overall']
            overall = math.floor(overall)
            # solve case where sentence.sentence (no space after period)
            text = re.sub(r'([a-z|\d])[\.]+([A-Z|\d|0-9|-])', r'\1. \2', text)
            text = re.sub(r'([a-z|\d])[\,]+([A-Z|\d|0-9|-])', r'\1, \2', text)
            text = nltk.word_tokenize(text)
            writer = None  # file writer

            if overall < 3:
                writer = neg_file
                neg += 1
            elif overall > 3:
                writer = pos_file
                pos += 1
            elif overall == 3:
                writer = neu_file
                neu += 1
            text = ' '.join(text)
            writer.write(text + ' ')
            writer.write('\n')
            counter += 1
            if counter % 100000 == 0:
                print counter
    print ('all done')
    print ('total ' + str(counter))
    print ('pos ' + str(pos))
    print ('neg ' + str(neg))
    print ('neu ' + str(neu))
    pos_file.close()
    neu_file.close()
    neu_file.close()
    t2 = time.time()
    print('time %d' % (t2 - t1))

if __name__ == '__main__':
    read_amazon_joblib()
