import torch
import torch.nn as nn
from torch.autograd import Variable

x1 = torch.rand(4,1,20)

x1 = Variable(x1)
c1 = nn.Conv1d(1, 1, 20)
c11 = nn.Conv1d(1, 1, 2)

o1 = c1(x1)

print(o1.size())
o1 = o1.squeeze()
print(o1.size())
o1 = o1.unsqueeze(0).unsqueeze(0)
print(o1.size())
o2 = c11(o1)
print(o2.size())

################
x2 = torch.rand(1, 1, 4, 20)
x2 = Variable(x2)
c2 = nn.Conv2d(1,1, (2, 20))
oo = c2(x2)
print(oo.size())