from multiprocessing import Pool
from joblib import Parallel, delayed


def f(i):
    x, y = i
    return x+y

def unwrap(i):
    self, ip = i
    return self.add(ip)

class Box:
    def __init__(self, num):
        self.b = []
        self.num = num

    def add(self, target):
        self.b.append(target)
        print ('add target %d'%(target))
        return target+self.num

if __name__ == '__main__':
    p = Pool(4)
    x = range(1,10,1)
    y = range(15, 20, 1)
    xy = zip(x, y)
    r = p.map(f, xy)

    box = Box(3)

    wrap = zip([box]*len(x),x)
    r = p.map(unwrap, wrap)


    print(r)
    print(box.b)

    boxj = Box(10)
    wrap = zip([boxj]*len(x), x)
    rj = Parallel(n_jobs=4)(delayed(unwrap)(x) for x in wrap)
    print (rj)