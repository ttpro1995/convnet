import argparse
def parsearg():
    parser = argparse.ArgumentParser(description='convnet')
    parser.add_argument('--name', default='default_name')
    parser.add_argument('--saved', default='saved')
    parser.add_argument('--logs', default='logs')
    parser.add_argument('--data', default='test_dataset',
                        help='data path')

    parser.add_argument('--embedding', default='../treelstm.pytorch/data/glove/')
    parser.add_argument('--optim', default='adam_combine')
    parser.add_argument('--lr', type=float, default=0.001)
    parser.add_argument('--epochs', type=int, default=10)
    parser.add_argument('--wd', type=float, default=1e-6)
    parser.add_argument('--emblr', type=float, default=0.001)
    parser.add_argument('--embwd', type=float, default=1e-6)
    parser.add_argument('--embedding_dim', type=int, default=20)
    parser.add_argument('--cuda', type=int, default=1)
    args = parser.parse_args()
    return args